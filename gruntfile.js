// Gruntfile.js
module.exports = grunt => {

  /**
   * CONFIGURATION
   */
  grunt.initConfig({
    doof: {
      src: ['src/doof.js'],
      options: {
        option1: true,
        option2: false
      }
    },
    goof: {
      reebok: {
        src: ['src/goof/*.js'],
        options: {
          option1: true,
          option2: false,
          option3: 'reebok'
        }
      },
      nike : {
        src: ['src/goof/**/*.js'],
        options: {
          option3: 'nike'
        }
      }
    },
    spoof: {
      files: [
        {
          expand: true,     // Enable dynamic expansion.
          cwd: 'lib/',      // Src matches are relative to this path.
          src: ['**/*.js'], // Actual pattern(s) to match.
          dest: 'build/',   // Destination path prefix.
          ext: '.min.js',   // Dest filepaths will have this extension.
          extDot: 'first'   // Extensions in filenames begin after the first dot
        },
      ]
    }
  });

  /**
   * LOAD NPM TASKS
   */
  grunt.loadNpmTasks('grunt-contrib-doof');
  grunt.loadNpmTasks('grunt-contrib-goof');
  grunt.loadNpmTasks('grunt-contrib-spoof');

  /**
   * REGISTER CUSTOM TASKS
   */

  grunt.registerTask('reebok', ['goof:reebok', 'spoof']);
  grunt.registerTask('nike', ['doof', 'goof:nike']);
  grunt.registerTask('default', ['reebok', 'nike']);

};
